from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import SalesPerson, SaleRecord, PotentialCustomer, AutomobileVO, SalesDetail
from django.http import JsonResponse
# Create your views here.

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "import_href"
        ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number",
    ]


class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = [
        "id",
        "name",
        "address",
        "number",
    ]


class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "id",
        "price",
        "sales",
        "customer",
        "automobile",
    ]
    encoders = {
        "sales": SalesPersonEncoder(),
        "customer": PotentialCustomerEncoder(),
        "automobile": AutomobileVODetailEncoder(),
    }

class SalesDetailEncoder(ModelEncoder):
    model = SalesDetail
    properties = [
        "sales",
        "customer",
        "vin",
        "price"
    ]
    encoders = {
        "sales": SalesPersonEncoder(),
        "customer": PotentialCustomerEncoder(),
        "vin": AutomobileVODetailEncoder(),
        "price": SaleRecordEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_sales(request):

    if request.method == "GET":
        sales = SalesPerson.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesPersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sale = SalesPerson.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the salesperson"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "POST"])
def api_list_customers(request):

    if request.method == "GET":
        customers = PotentialCustomer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=PotentialCustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = PotentialCustomer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=PotentialCustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_list_record(request):
    if request.method == "GET":
        records = SaleRecord.objects.all()
        return JsonResponse(
            {"records": records},
            encoder=SaleRecordEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_id = content["sales_id"]
            sales = SalesPerson.objects.get(id=sales_id)
            content["sales"] = sales
            customer_id = content["customer_id"]
            customer = PotentialCustomer.objects.get(id=customer_id)
            content["customer"] = customer
            automobile_id = content["automobile_id"]
            automobile = AutomobileVO.objects.get(id = automobile_id)
            content["automobile"] = automobile
            record = SaleRecord.objects.create(**content)
            return JsonResponse(
                record,
                encoder=SaleRecordEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the sales record"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET"])
def api_list_sales_detail(request, pk):
    if request.method == "GET":
        records = SaleRecord.objects.filter(sales=pk)
        return JsonResponse(
            {"records": records},
            encoder=SaleRecordEncoder
        )
