from django.urls import path
from .views import (
    api_list_sales,
    api_list_customers,
    api_list_record,
    api_list_sales_detail

)

urlpatterns = [
    path("sales/", api_list_sales, name="api_list_sales"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("records/", api_list_record, name="api_list_record"),
    path("records/<int:pk>", api_list_sales_detail, name="api_list_sales_detail"),


]
