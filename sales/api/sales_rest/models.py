from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=200)


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=200)

class PotentialCustomer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    number = models.CharField(max_length=200)

class SaleRecord(models.Model):
    price = models.IntegerField()

    sales = models.ForeignKey(
        SalesPerson,
        related_name="sales_person",
        on_delete=models.CASCADE,
    )

    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="customer",
        on_delete=models.CASCADE,
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE,
    )

class SalesDetail(models.Model):
    sales = models.ForeignKey(
        SalesPerson,
        related_name="sales_persons",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="customers",
        on_delete=models.CASCADE,
    )

    vin = models.ForeignKey(
        AutomobileVO,
        related_name="vin_num",
        on_delete=models.CASCADE,
    )

    price = models.ForeignKey(
        SaleRecord,
        related_name="prices",
        on_delete=models.CASCADE,
    )
