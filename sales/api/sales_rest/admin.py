from django.contrib import admin
from .models import AutomobileVO, SaleRecord, SalesPerson, PotentialCustomer, SalesDetail
# Register your models here.

@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    pass

@admin.register(SaleRecord)
class SaleRecordAdmin(admin.ModelAdmin):
    pass

@admin.register(SalesPerson)
class SalesPersonAdmin(admin.ModelAdmin):
    pass

@admin.register(PotentialCustomer)
class PotentialCustomerAdmin(admin.ModelAdmin):
    pass

@admin.register(SalesDetail)
class SalesDetailAdmin(admin.ModelAdmin):
    pass
