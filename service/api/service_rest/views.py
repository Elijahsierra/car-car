from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment
import json

# Create your views here.

class TechnicianDetailEnocder(ModelEncoder):
    model = Technician
    properties = [
        'id',
        'name',
        'employee_number',
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'id',
        'vin',
        'owner',
        'date',
        'time',
        'technician',
        'reason',
        'is_vip',
        'completed',
    ]

    encoders = {
        "technician": TechnicianDetailEnocder(),
    }

    # def get_extra_data(self, o):
    #     return {"date": o.date, "time": o.time
    #     }



@require_http_methods(['GET', 'POST'])
def api_list_technicians(request):
    if request.method == 'GET':
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianDetailEnocder
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEnocder,
                safe=False
            )
        except:
            return JsonResponse(
                {"message": "Failed to register Technician"},
                status=400
            )


@require_http_methods(['GET', 'DELETE', 'PUT'])
def api_show_technician(request, pk):
    if request.method == 'GET':
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEnocder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {'message': "Invalid ID Number"},
                status=400
            )
    elif request.method == 'DELETE':
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({'Deleted': count > 0})
    else:
        content = json.loads(request.body)
        try:
            Technician.objects.filter(id=pk).update(**content)
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEnocder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid ID Number'}
            )


@require_http_methods(['GET', 'POST'])
def api_list_appointments(request, vin=None):
    if request.method == 'GET':
        if vin == None:
            appointments = Appointment.objects.all()
            return JsonResponse(
                {"appointments": appointments},
                encoder=AppointmentEncoder
            )
        else:
            try:
                appointments = Appointment.objects.filter(vin=vin)
                return JsonResponse(
                    {"appointments": appointments},
                    encoder=AppointmentEncoder
                )
            except Appointment.DoesNotExist:
                return JsonResponse(
                    {"message": "Appointment Not Found"}
                )
    else:
        content = json.loads(request.body)
        try:
            employee_id = content["technician"]
            technician = Technician.objects.get(id=employee_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician ID"}
            )

        if AutomobileVO.objects.filter(vin=content["vin"]).exists(): #checking to see if vin matches with existing vin
            content["is_vip"] = True
        else:
            content["is_vip"] = False

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointments(request, pk):
    if request.method == 'GET':
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                {'appointments': appointment},
                encoder=AppointmentEncoder,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment Not Found"},
                status=400,
            )
    elif request.method == 'DELETE':
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse(
            {'deleted': count > 0}
        )
    else:
        try:
            content = json.loads(request.body)
            Appointment.objects.filter(id=pk).update(**content)
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {'message': "Appointment Not Found"},
                status=400,
            )
