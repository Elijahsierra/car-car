from django.db import models
from django.urls import reverse

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)

    def get_api_url(self):
        return reverse("api_show_appointments", kwargs={"vin": self.vin})


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_api_urls(self):
        return reverse('api_list_technician', kwargs={'pk': self.id})


class Appointment(models.Model):
    vin = models.CharField(max_length=17, null=True)
    owner = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT
    )
    reason = models.CharField(max_length=200)
    is_vip = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)

    def __str__(self):
        return self.owner
