import { useEffect, useState } from "react";




const SalesRecordList = (props) => {
    const [records, setRecords] = useState([]);
    const [og, setog] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8090/api/records/')
            .then(response => response.json())
            .then(data => {
                setRecords(data.records)
                setog(data.records);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    const search = (id) => {
        const sales = og.filter((test) => {
        return test.sales.id === Number(id)
        })
        setRecords(sales)

    }

    return (
        <div>
            <h1>Sales Person History</h1>
            <div className="mb-3">
                  <select required onChange={(e) => search(e.target.value)}
                    name="sale" id="sale" className="form-select">
                    <option value="">Choose a Sales Person</option>
                    {props.sales.map(sale => {
                        return (
                        <option key={sale.id} value={sale.id}>
                            {sale.name}</option>
                        )
                    })}
                </select>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Customer</th>
                            <th>Vin</th>
                            <th>Sale Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    {records.map(record => {
                            return (
                                <tr key={record.id}>
                                    <td>{record.sales.name}</td>
                                    <td>{record.customer.name}</td>
                                    <td>{record.automobile.vin}</td>
                                    <td>{record.price}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
        </div>
    </div>
    );
};

export default SalesRecordList;
