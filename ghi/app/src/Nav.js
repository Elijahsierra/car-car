import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <div className="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            Manufacturers
            </button>
            <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton1">
            <li className="dropdown-item">
              <NavLink className="nav-link active" aria-current="page" to="/manufacturers">Manufacturer List</NavLink>
            </li>
            <li className="dropdown-item">
              <NavLink className="nav-link active" aria-current="page" to="/manufacturers/new">New Manufacturer</NavLink>
            </li>
            </ul>
          </div>
          <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            Models
            </button>
            <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton1">
            <li className="dropdown-item">
              <NavLink className="nav-link active" aria-current="page" to="/models">Vehicle Model List</NavLink>
            </li>
            <li className="dropdown-item">
              <NavLink className="nav-link active" aria-current="page" to="/models/new">New Vehicle Model</NavLink>
            </li>
            </ul>
          </div>
          <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            Inventory
            </button>
            <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton1">
            <li className="dropdown-item">
            <NavLink className="nav-link active" aria-current="page" to="/automobiles">Inventory List</NavLink>
            </li>
            <li className="dropdown-item">
            <NavLink className="nav-link active" aria-current="page" to="/automobiles/new">Add New Automobile to Inventory</NavLink>
            </li>
            </ul>
          </div>
          <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            Sales
            </button>
            <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton1">
            <li className="dropdown-item">
            <NavLink className="nav-link active" aria-current="page" to="/sales/new">New Sales Person</NavLink>
            </li>
            <li className="dropdown-item">
            <NavLink className="nav-link active" aria-current="page" to="/customer/new">New Customer</NavLink>
            </li>
            <li className="dropdown-item">
            <NavLink className="nav-link active" aria-current="page" to="/records/new">New Sales Record</NavLink>
            </li>
            <li className="dropdown-item">
            <NavLink className="nav-link active" aria-current="page" to="/records">Sales Person History</NavLink>
            </li>
            </ul>
          </div>
          <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            Technician
            </button>
            <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton1">
            <li className="dropdown-item">
            <NavLink className="nav-link" to="technicians/add">Technician Form</NavLink>
            </li>
            </ul>
          </div>
          <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            Appointments
            </button>
            <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton1">
            <li className="dropdown-item">
            <NavLink className="nav-link" to="appointments/">Appointments</NavLink>
            </li>
            <li className="dropdown-item">
            <NavLink className="nav-link" to="appointments/add">Appointment Form</NavLink>
            </li>
            <li className="dropdown-item">
            <NavLink className="nav-link" to="appointments/history">Service History</NavLink>
            </li>
            </ul>
          </div>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
