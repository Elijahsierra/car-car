import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadData() {
  const response = await fetch('http://localhost:8100/api/manufacturers/');
  const response2 = await fetch('http://localhost:8100/api/models/')
  const response3 = await fetch('http://localhost:8100/api/automobiles/')
  const response4 = await fetch('http://localhost:8090/api/sales/')
  if (response.ok) {
    const data = await response.json();
    const data2 = await response2.json();
    const data3 = await response3.json();
    const data4 = await response4.json();
    root.render(
      <React.StrictMode>
        <App manufacturers={data.manufacturers} models={data2.models} autos={data3.autos} sales={data4.sales}/>
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadData();
