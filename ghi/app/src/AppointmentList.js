import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const AppointmentList = () => {
    const [appointments, setAppointments] = useState([])

    useEffect(() => {
        fetch('http://localhost:8080/api/appointments/')
        .then(response => response.json())
        .then(data => {
            setAppointments(data.appointments)
        })
        .catch(e => console.log('error:', e))
    }, [])

    const onDeleteAppointment = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            window.location.reload()
        }
    }

    const appointmentCompleted = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/`
        const fetchConfig = {
            method: 'put',
            body: JSON.stringify({ "completed": true }),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {

            window.location.reload()
        }
    }

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Owner</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Vip</th>
                        <th>Complete</th>
                        <th>Cancel</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        if (!appointment.completed) {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.owner}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.time}</td>
                                    <td>{appointment.technician.name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.is_vip ? '√': "x"}</td>
                                    <td>
                                        <button onClick={(e) => appointmentCompleted(appointment.id)}>completed</button>
                                        <span>{appointment.model}</span>
                                    </td>
                                    <td>
                                        <button onClick={() => onDeleteAppointment(appointment.id)}>Cancel</button>
                                        <span>{appointment.model}</span>
                                    </td>
                                </tr>
                            )
                        }
                    })}
                </tbody>
            </table>
            <Link to={"/shoes/new"}>Add An Appointment</Link>
        </>
    );
};

export default AppointmentList;
